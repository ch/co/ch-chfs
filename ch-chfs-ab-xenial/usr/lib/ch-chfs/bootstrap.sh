#!/bin/bash
 
set -e
zfs create data/group
zfs create data/config
zfs create -o refquota=100GB data/reserved
(cd `dirname $0`/bootstrap.dir && tar -cf - .) | (cd /data/config && tar -xf -)
mkdir /data/config/home
mv /home /oldhome
ln -s /data/config/home /home
