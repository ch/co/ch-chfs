#!/bin/bash
L=$1
GROUP=${L/-fs/}
[ -z $GROUP ] && exit 1
SHARES=/data/group/$GROUP/config/samba/shares
SHARELIST=/data/group/$GROUP/config/samba/.sharelist
TEMP=` mktemp -t SMB-$GROUP-XXXX`
if test $SHARES -nt $SHARELIST ; then
 ls -1 $SHARES | sed 'sZ^Z include = '$SHARES/Z >>$TEMP
 mv $TEMP $SHARELIST
fi
