#!/bin/bash
set -e
. `dirname $0`/config


function setup_disk {
    disk=$1
    dev=/dev/$1
    echo parted -s $dev mklabel gpt 
    echo parted -s $dev mkpart primary 0 2mb
    echo parted -s $dev mkpart primary 2mb 100\%
    echo parted -s $dev set 1 bios_grub on
}

function create_drbd {
    number=$#
    i=0
    while [[ $i -lt $number ]] ; do
        disk=$1
        shift
        write_drbd_file $i $disk 
        init_drbd $num
        i=$((i+1))
    done
}

function write_drbd_file {
    firstport=7789
    num=$1
    disk=$2

    dir=/etc/drbd.d
    filename=${dir}/drbd${num}.res
    port=$((firstport+i))
    d=$(date)
    
    cat > $filename <<EOF
# DRBD config file created on $d
# by $0
resource "drbd${num}" {
 syncer {
  # Priority 1
  # First or second DRBD
 }
 on $MYNAME {
  device        /dev/drbd${num};
  disk          /dev/${disk}2;
  address       ${MYREPNETIP}:${port};
  meta-disk     internal;
 }
EOF
    if [ -n "$TWINREPNETIP" ] ; then
    cat >> $filename <<EOF
 on $TWINNAME {
   device       /dev/drbd${num};
   disk         /dev/${disk}2;
   address      ${TWINREPNETIP}:${port};
   meta-disk    internal;
 }
EOF
    fi
    cat >> $filename <<EOF
}
EOF
}

function set_names {
    MYNAME=$(hostname -s)
    STEM=$(echo $MYNAME | sed 's/\(.*\)-[^-]*/\1/')
    EXTENSION=$(echo $MYNAME | sed 's/.*-\([^-]*\)/\1/')
    TWINEXTENSION=$(echo $EXTENSION | tr 'ab' 'ba')
    TWINNAME=${STEM}-${TWINEXTENSION}
    MYREPNETIP=$(getent hosts ${MYNAME}.repnet.private.cam.ac.uk | awk '{print $1}')
    TWINREPNETIP=$(getent hosts ${TWINNAME}.repnet.private.cam.ac.uk | awk '{print $1}')

    if [ -z "$MYREPNETIP" ] ; then
        MYREPNETIP=$(getent hosts $MYNAME | awk '{print $1}')
    fi
    export MYNAME STEM EXTENSION TWINEXTENSION TWINNAME MYREPNETIP TWINREPNETIP
}

function init_drbd {
 set -x
 num=$1
 echo yes | drbdadm create-md drbd${num}
 if [ -n "$TWINREPNETIP" ] ; then
     echo yes | ssh $TWINREPNETIP drbdadm create-md drbd${num}
 fi
 drbdadm up drbd${num}
 if [ -n "$TWINREPNETIP" ] ; then
     ssh $TWINREPNETIP drbdadm up drbd${num}
 fi
 if [ -n "$TWINREPNETIP" ] ; then
     DEVICE=`drbdadm dump drbd${num} | grep -A2 $MYNAME | grep device | awk ' { print $2 ; } ' | sed s/\;//`
     drbdsetup $DEVICE primary --overwrite-data-of-peer
 else
     drbdadm primary --force drbd${num}
 fi
 set +x
}

echo 'This script does not partition disks, but prints out commands you might want to run to partition disks.'
echo 'However it WILL overwrite your drbds'
echo 'It is not well-tested so probably better read it first'
countdown 10

set_names

for disk in "$@"
do
  setup_disk $disk
  create_drbd "$@"
done
