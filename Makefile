# A Makefile to set up a Debian machine
# Related to configuration of debian machines in Chemistry
IFACED:=/etc/network/interfaces.d
IFACES:=/etc/network/interfaces
IPMITOOL:=/usr/bin/ipmitool
XM:=/usr/sbin/xm
IPMIDEV:=/dev/ipmi0
IPMIPASSFILE:=/etc/ipmi-password
GRUBDEFAULT:=/etc/default/grub
DRBDADM:=/sbin/drbdadm
DRBDPROC:=/proc/drbd
REPNET:=repnet.private.ch.cam.ac.uk
REPNETSN:=/24
ROOTSSHPUBKEY:=/root/.ssh/id_rsa.pub

# Related to this machine
ETH0IPCIDR:=$(shell ip address show dev eth0 | awk ' $$1 == "inet" {print $$2 }' | head -1)
ETH0IPBCAST:=$(shell ip address show dev eth0 | awk ' $$1 == "inet" {print $$4 }' | head -1)
ETH0GW:=$(shell route -n | awk ' $$1 == "0.0.0.0" && $$8 == "eth0" { print $$2 } ' )
HOSTNAME:=$(shell hostname -s)
REPNETHN:=$(HOSTNAME).$(REPNET)
ETH1IP:=$(shell getent hosts $(REPNETHN) | awk ' { print $$1 } ')
GRUBXEN:=GRUB_CMDLINE_XEN="dom0_mem=1024M,max:1024M dom0_max_vcpus=2 dom0_vcpus_pin"
SSHQQ:=ssh -i $(ROOTSSHPUBKEY) -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o Batchmode=yes twin

# Related to this makefile
# When set, prevent any prompts for variables
SMAKE=make --no-print-directory

# Display (this) help text
help: export NONINTERACTIVE:=true
# undoc
help:
	$(info Valid targets are:)
	@awk $(SELFAWK) Makefile
	@true

# Show low-level targets (useful if extending this Makefile)
lowlevelhelp:
	$(info Low-level targets used internally are:)
	@awk $(EXTRAAWK) Makefile


# Bit of a test target, really
all: $(IFACES) $(IFACED)/eth0.cfg

# create configuration for eth0 
eth0: $(IFACES) $(IFACED)/eth0.cfg

# create configuration for eth1
eth1: $(IFACES) $(IFACED)/eth1.cfg

# To write the configuration for eth0 to a separate file
$(IFACED)/eth0.cfg: $(IFACED)/. $(IFACES)
ifeq ($(ETH0IPCIDR),)
	$(error Could not find IP address for eth0)
endif
ifeq ($(ETH0IPBCAST),)
	$(error No broadcast address found for eth0)
endif
ifeq ($(ETH0GW),)
	$(error No gateway found for eth0)
endif
ifeq ($(wildcard $(IFACED)/eth0.cfg),)
	$(info Sorting configuration for eth0)
	@echo "$$ETH0" >$(IFACED)/eth0.cfg
else
	$(info $(IFACED)/eth0.cfg already exists)
endif

# To write the repnet interface $(IFACED)
$(IFACED)/eth1.cfg:
ifeq ($(ETH1IP),)
	$(error Could not find IP address for eth1)
endif
ifeq ($(wildcard $(IFACED)/eth1.cfg),)
	$(info Sorting configuration for eth1)
	@echo "$$ETH1" >$(IFACED)/eth1.cfg
else   
	$(info $(IFACED)/eth1.cfg already exists)
endif

# To adjust Debian's standard network/interfaces file to 
# something more sustainable
$(IFACES):
ifeq ($(shell grep '^source $(IFACED)/..cfg' $(IFACES)),)
	$(info Adding subdirectory inclusion to $(IFACES))
	echo 'source $(IFACED)/*.cfg' >>$(IFACES)
endif
ifneq ($(shell grep '. The primary network interface' $(IFACES)),)
	$(info Removing guff from $(IFACES))
	sed -i 's/. The primary network interface//' $(IFACES)
endif
ifneq ($(shell grep 'eth0' $(IFACES)),)
	@sed s/.*eth0.*// -i $(IFACES)
endif


# To test for twin
ifeq ($(shell grep twin /etc/hosts),)
# To set up ssh to twin
twin-ssh:
	$(error Twin not defined in /etc/hosts)

#undoc
twin: 
	@TWIN=$(TWIN); $(SMAKE) twin-real TWIN=$${TWIN:-`read -p "Enter name of twin: " T; echo $$T`}

#undoc
twin-real: TWINHOSTLINE:=$(shell getent hosts $(TWIN).$(REPNET))
#undoc
ifeq ($(TWIN),) 
# undoc
twin-real:
	$(error TWIN not defined)
else
# undoc
twin-real: 
	@[ ! -z "$(TWIN)" ]
	@if [ -z "$(TWINHOSTLINE)" ] ; then \
	echo Twin hostname $$TWIN.$(REPNET) cannot be found;\
	exit 2; \
	fi
	@echo $(TWINHOSTLINE) twin >>/etc/hosts
	$(info Added twin to /etc/hosts)
endif
else
#undoc
twin-ssh: $(ROOTSSHPUBKEY) ssh-copykey

# Configure twin's entry in /etc/hosts
twin:
	$(info Twin already present in /etc/hosts)
endif

ifeq ($(shell $(SSHQQ) echo OK 2>/dev/null),OK)
# undoc
ssh-copykey:
	$(info Key access already set up)
else
# xdoc Copy SSH key to twin
ssh-copykey:
	$(info Key access not already set up)
	@ssh-copy-id -i $(ROOTSSHPUBKEY) twin
endif
	
$(ROOTSSHPUBKEY):
	[ ! -f "$(ROOTSSHPUBKEY)" ] && ssh-keygen -t rsa

# Configure the IPMI controller
ipmi: $(IPMITOOL) $(IPMIDEV) ipminet ipmipass

#xdoc Configure IPMI networkinig
ipminet:
	$(info Running helpers/ipminet to configure IPMI networking)
	@./helpers/ipmisetnet
	
#xdoc Configure IPMI password/access
ipmipass: $(IPMIPASSFILE)
	@$(IPMITOOL) lan set 1 access on
	@$(IPMITOOL) lan set 1 auth ADMIN MD5,PASSWORD
	@$(IPMITOOL) user set name 2 root
	@$(IPMITOOL) user set password 2 $(shell cat $(IPMIPASSFILE))
	@$(IPMITOOL) user enable 2
	@$(IPMITOOL) channel setaccess 1 2 callin=on ipmi=on link=on privilege=4
	@$(IPMITOOL) sol payload enable 1 2
	@$(IPMITOOL) sol set enabled true 1
	$(info Checking network access to IPMI controller)
	$(IPMITOOL) -I lan -H $(shell hostname).ipmi.private.ch.cam.ac.uk -U root -P$(shell cat $(IPMIPASSFILE)) chassis status

# A reminder to create the IPMI password file
$(IPMIPASSFILE):
	$(info Create $(IPMIPASSFILE) with the IPMI password)

# xdoc Configure the IPMI device
$(IPMIDEV):
	@grep -q ipmi_si /etc/modules || echo 'ipmi_si' >>/etc/modules
	@grep -q ipmi_devintf /etc/modules || echo 'ipmi_devintf' >>/etc/modules
	@modprobe ipmi_si
	@modprobe ipmi_devintf
	@[ -c $(IPMIDEV) ] || (echo Unable to create IPMI device; exit 2)

#xdoc Install the ipmi utilities
$(IPMITOOL):
	apt-get install ipmitool -y

# To configure a XEN Dom0
dom0: grubxen $(XM) xenrunning

#undoc Check Xen is running
ifeq ($(shell xm info >/dev/null 2>/dev/null || echo BAD),)
#undoc
xenrunning:
	$(info Xen is running)
else
#xdoc Test Xen is running
xenrunning:
	$(error Xen is not running. Reboot?)
endif

#xdoc Grub Xen command line
grubxen:
ifeq ($(shell . /$(GRUBDEFAULT) && echo $$GRUB_CMDLINE_XEN),)
	@echo '$(GRUBXEN)' >>/$(GRUBDEFAULT)
	@update-grub
	$(info Added GRUB_CMDLINE_XEN to grub config)
else
	$(info GRUB_CMDLINE_XEN already set)
endif
ifeq ($(shell . /$(GRUBDEFAULT) && echo $$GRUB_DISABLE_OS_PROBER),true)
	$(info OS Prober already disabled)
else
	@echo 'GRUB_DISABLE_OS_PROBER=true' >>/$(GRUBDEFAULT)
	@update-grub
endif
	
#undoc
ifeq ($(shell bash -c "vgs --noheadings" 2>/dev/null| wc -l),1)
lv: VGROUP:=$(shell bash -c "vgs --noheadings" 2>/dev/null | awk ' { print $$1 } ')
endif
# To create a logical volume
lv:
	@LVNAME=$(LVNAME) LVSIZE=$(LVSIZE) VGROUP=$(VGROUP); \
	$(SMAKE) lv-real LVNAME=$${LVNAME:-`read -p 'Enter logical volume name: ' T ; echo $$T`} \
	LVSIZE=$${LVSIZE:-`read -p 'Enter logical volume size: ' T ; echo $$T` } \
	VGROUP=$${VGROUP:-`read -p 'Enter volume group name: ' T ; echo $$T`}
	

# xdoc Create a logical volume
lv-real:
	[ ! -z "$(LVNAME)" ]
	[ ! -z "$(LVSIZE)" ]
	[ ! -z "$(VGROUP)" ]
	@lvcreate -n "$(LVNAME)" -L $(LVSIZE) $(VGROUP)

#undoc Install Xen utilities
$(XM):
	$(info Installing xen)
	@dpkg-divert  --divert /etc/grub.d/50_linux --rename --add  /etc/grub.d/10_linux
	apt-get install -y xen-linux-system-amd64


# To configure DRBD
drbd: $(DRBDADM) $(DRBDPROC)
 
# xdoc Install the drbd utilities
$(DRBDADM):
	@apt-get install drbd8-utils

# xdoc Start DRBD
$(DRBDPROC):
	@sed -i "s/usage-count yes/usage-count no/" /etc/drbd.d/global_common.conf
	@/etc/init.d/drbd start

hobbit:
	$(info Configuring Hobbit)
ifeq ($(shell [ -f $(XM) ] && echo OK),OK)
	apt-get install hobbit-chem-dom0
endif

# TODO:
# hobbit
# Display a quick summary of where we are
status:
# Check related to eth0
ifeq ($(shell [ -f $(IFACED)/eth0.cfg ] && echo OK),OK)
	$(info + Eth0 configured OK)
else
	$(info - Eth0 not configured - make eth0)
endif
# Checks related to the repnet
ifeq ($(shell [ -f $(IFACED)/eth1.cfg ] && echo OK),OK)
	$(info + Eth1 configured OK)
 ifneq ($(shell grep twin /etc/hosts),)
	$(info + Twin configured in /etc/hosts)
  ifeq ($(shell $(SSHQQ) echo OK 2>/dev/null),OK)
	$(info + SSH access to twin set up)
  else
	$(info - SSH access to twin not set up)
  endif 
  ifeq ($(shell [ -f /proc/drbd ] && echo OK),OK)
	$(info + DRBD present)
  else
	$(info - DRBD not present)
  endif
 else
	$(info - Twin not configured in /etc/hosts - make twin)
 endif
endif
# Checks related to Xen/Dom0
ifeq ($(shell [ -f $(XM) ] && echo OK),OK)
	$(info + XM installed)
 ifeq ($(shell xm info >/dev/null 2>/dev/null || echo BAD),)
	$(info + Xen running)
 else
	$(info - Xen not running - reboot?)
 endif
else
	$(info - XM not installed - make $(XM))
endif
# Checks related to IPMI?
ifeq ($(shell [ -c $(IPMIDEV) ] && echo OK),OK)
	$(info + IPMI installed)
else
	$(info - IPMI not installed - make ipmi)
endif
	@true

# xdoc To create a directory:
%/.:
	$(info Making directory $*)
	@mkdir -p "$*"




# Definitions


define ETH0
# A configuration for the primary network interface - auto generated on $(shell date)
# Converted from address assigned by DHCP after first build
auto eth0

iface eth0 inet static
        address $(ETH0IPCIDR)
        broadcast $(ETH0IPBCAST)
        gateway $(ETH0GW)
endef
export ETH0

define ETH1
# A configuration for the repnet network interface - auto generated on $(shell date)
# IP address discovered via DNS

auto eth1
iface eth1 inet static
	address $(ETH1IP)/$(REPNETSN)
endef
export ETH1

### Default target is to display some help
define SELFAWK 
"\
/^[A-z0-9.%\-]*:/  && ! /^_/ && ! /:=/ && ! /^\./ {\
 sub (/%/,\"\$$ARG\",\$$1); \
 sub (/\# /,\"\",line); \
 sub (/^doconly-.*/,\"*\",\$$1); \
 if (line !~/xdoc/ && line !~/undoc/) {\
  print \"\",\$$1,line\
 }\
} ; \
 {line=\$$0}"
endef
define EXTRAAWK 
"\
/^[A-z0-9.%\-]*:/  && ! /^_/ && ! /:=/ && ! /^\./ {\
 sub (/%/,\"\$$ARG\",\$$1); \
 sub (/\# /,\"\",line); \
 sub (/^doconly-.*/,\"*\",\$$1); \
 if (line ~/xdoc/ && line !~/undoc/) {\
  sub (/xdoc/,\"\",line); \
  print \"\",\$$1,line\
 }\
} ; \
 {line=\$$0}"
endef


.PHONY: $(IFACES) help eth0 eth1 ipminet grubxen lv twin ssh-copykey status
.SECONDARY: $(IFACES) $(IFACED)/eth0.cfg $(IFACED)/eth1.cfg $(ROOTSSHPUBKEY) 
