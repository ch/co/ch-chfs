#!/bin/bash
# To upload a package to downloads.ch
DOWNLOADS=downloads.ch.private.cam.ac.uk
PKG="$1"
DIST="$2"
if [ -z $DIST ] ; then
 echo Useage: $0 packagename distro
 exit 1
fi

DEB=`ls -at deb-packages/$PKG* | head -1`
echo Uploading $DEB

scp $DEB root@$DOWNLOADS:/tmp/
ssh root@$DOWNLOADS make -C /var/www/local-debs debtodist-$DIST DEB=/tmp/`basename $DEB`
