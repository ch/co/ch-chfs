#!/bin/bash
if [ -z $1 ] ; then
 echo Use: $0 package-name
 exit 1
fi

D=`dirname $0`

# Find old version
DIR=$1
CONTROL="$D/$DIR/DEBIAN/control"

# Canonical package name
PKG=`grep ^Package: $CONTROL | awk ' { print $2 } '`
OVER=`grep Version: $CONTROL | awk ' { print $2 } '`
CVER=`echo $OVER | sed s/.*ch//`
NVER=`echo $OVER | sed s/ch$CVER$/ch$((CVER+1))/`
ARCH=`grep Architecture: $CONTROL | awk ' { print $2 } '`
sed -i "s/Version: $OVER/Version: $NVER/" $CONTROL
mkdir -p $D/deb-packages

# Build the package
fakeroot dpkg-deb -b $D/$DIR $D/deb-packages/${PKG}_${NVER}_${ARCH}.deb || echo Failed
