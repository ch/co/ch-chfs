#!/bin/bash

. `dirname $0`/config
echo Creating zpool $BASEZFS - CTRL-C to cancel
countdown 5
zpool create $BASEZFS raidz2 /dev/drbd/by-res/*
