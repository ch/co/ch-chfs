#!/bin/bash

# Destroys DRBDs and magically start them synchronized. 
# But empty!

if [ "$1" != "yes I mean it" ] ; then
 echo Read the script first.
 exit 1
fi
. `dirname $0`/config
set -e

echo '-----------------------------------------------------'
echo ' BIG FAT WARNING - BIG FAT WARNING - BIG FAT WARNING '
echo ' * About to destroy all DRBDs. CTRL-C now to abort * '
echo ' * * * * * You have ten seconds to abort * * * * * * '
echo '-----------------------------------------------------'
countdown 10

echo Bringing down all resources
drbdadm down all
ssh twin drbdadm down all

for RES in `drbdadm dump | grep ^resource | awk ' { print $2 } '`; do
 echo Creating new metadata for $RES
 drbdadm -- --force create-md $RES
 ssh twin drbdadm -- --force create-md $RES
 echo Bringing up $RES
 drbdadm up $RES
 ssh twin drbdadm up $RES
 echo -n Waiting for $RES to connect
 # Wait for inconsistent
 while ! ( drbdadm dstate $RES | grep -q Inconsistent/Inconsistent ) ; do
  sleep 1
  echo -n .
 done
 echo done
 echo Creating new UUID on $RES
 drbdadm -- --clear-bitmap new-current-uuid $RES
 echo Making $RES primary
 drbdadm primary $RES
done


