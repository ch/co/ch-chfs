#!/bin/bash
# To bring up IP addresses and VLANs for the linux fileserver

CONF='/data/config'
VLANINT=eth0
INTERFACE=eth0:0
VLANDIR=$CONF/vlans
IPDIR=$CONF/ips

# two possibilities here: we have a symlink (which must be to a samba config file
# so we testparm, or we have a file which must follow the naming convention of
# a.b.c.d_m where m is the mask length, e.g. 10.0.0.1_24 if you want 10.0.0.1/24
function getips() {
  DIR=$1
  for l in `find $DIR -type l`
  do
    target=`readlink $l`
    if [ -f "$target" ]
    then
      testparm -s $target --parameter-name interfaces 2>/dev/null
    fi
  done
  find $DIR -type f | xargs -n 1 basename | sed "s#_#/#" 
}

function checkip() {
   IP=$1
   INTERFACE=$2
   if ! isup $IP $INTERFACE; then
    NOIPS="$NOIPS,$IP"
   else
    if [ -n $RED ] ; then # we are doing a xymonstatus
     echo $GREEN IP $IP is up
    fi
   fi
}


function upall() {
  # IPs in default VLAN
  for IP in `getips $IPDIR` ; do
    ipup $IP $INTERFACE 
  done
  # Other VLANS
  upvlans
}

function isup() {
 WANTED=$1
 LINE=`ip addr show dev $INTERFACE 2>/dev/null | grep "inet $WANTED "`
 MYIP=`echo $LINE | awk '{print $2}'`
 [ "$WANTED" = "$MYIP" ]
}

function ipup () {
  UP=$1
  DEV=$2
  if isup $IP $DEV; then
    echo $IP is already up on $DEV
  else
    ip addr add $IP dev $DEV
    if isup $IP $DEV; then
      echo Brought up $IP on $DEV
    else
      echo Error bringing up $IP on $DEV
      return 1
    fi
  fi
}

function upvlans() {
  VLANS=`ls -1 $VLANDIR`
  for VLAN in $VLANS ; do
    DEV=vlan${VLAN}
    echo Bringing up VLAN $VLAN
    ensurevlandevice $VLAN
    VIPS=`getips $VLANDIR/$VLAN`
    for IP in $VIPS ; do
      ipup $IP DEV
    done
  done
}

function ensurevlandevice() {
 VLAN=$1
 DEV=vlan${VLAN}
 if ! ip link show $DEV  ; then
   ip link add link $VLANINT name $DEV type vlan id $VLAN 
 fi
}


#set -xv

case $1 in
 start)
  echo Bringing up IP addresses
  upall
 ;;
 stop)
  # remove IP aliases from default VLAN
  DEFAULTIPS=`getips $IPDIR`
  for IP in $DEFAULTIPS ; do
   if isup $IP ; then
    ip addr del $IP dev $INTERFACE
   else
    echo $IP not up on $INTERFACE
   fi
  done
  # remove vlan interfaces, this removes IPs and routes
  VLANS=`ls -1 $VLANDIR`
  for VLAN in $VLANS ; do
    DEV=vlan${VLAN}
    if ip link show $DEV 2>/dev/null ; then
      ip link del $DEV
    else
      echo VLAN $VLAN was not up
    fi
  done
 ;;
 reload)
  echo Checking for new IP addresses
  upall
 ;;
 status|xymonstatus)
  # for heartbeat status checking
  IS_OK=0
  if [ $1 = xymonstatus ] ; then
   RED='&red;'
   GREEN='&green;'
  fi
  NOIPS=""
  DEFAULTIPS=`getips $IPDIR`
  for IP in $DEFAULTIPS; do 
    checkip $IP $INTERFACE
  done
  VLANS=`ls -1 $VLANDIR`
  for VLAN in $VLANS ; do
    DEV=vlan${VLAN}
    VIPS=`getips vlans/$VLAN`
    for IP in $VIPS ; do
      checkip $IP $DEV
    done
  done
  if [ ! -z $NOIPS ] ; then
   echo $RED Missing IP addresses ${NOIPS/,/}
   IS_OK=1
  else
   echo $GREEN All IP addresses up
   IS_OK=0
  fi
  exit $IS_OK
 ;;
 *)
  echo Use: $0 '[start|stop|reload|status]'
 ;;
esac

