#!/bin/bash
# Adds a share to a group

function usage() {
 echo Use: $0 -g GROUP -d DIR -n NAME [-a] [-s] [-t] [-c]
 echo g GROUP - name of group
 echo d DIR - directory to share
 echo n NAME - name of share
 echo a - with ACL
 echo s - with SNAPshots
 echo t - with trash
 echo c - communal share
 exit 1
}
SHAREOPTS=""
while getopts ":g:d:n:astc" opt; do
   case $opt in
   g ) GROUP="$OPTARG" ;;
   d ) DIR="$OPTARG";;
   n ) NAME="$OPTARG";;
   a ) SHAREOPTS="$SHAREOPTS acl";;
   s ) SHAREOPTS="$SHAREOPTS backups";;
   t ) SHAREOPTS="$SHAREOPTS recycle";;
   c ) COMMUNAL=true ;;
   ? ) usage ;;
   \?)  usage ;;
   esac
done

. `dirname $0`/config

SHAREOPTS=$(echo $SHAREOPTS | tr " " "\n" | sort | tr "\n" " ")
SHAREOPTS=${SHAREOPTS% }
SHAREOPTS=${SHAREOPTS// /-}
SHAREOPTS=${SHAREOPTS/backups/snapshot}
 
if [ -z "$GROUP" -o -z "$DIR" -o -z "$NAME" ] ; then usage ; fi
shift $(($OPTIND - 1))

# Some sanity checks
# Does the group exist?
if ! zfs list ${GROUPZFS}/${GROUP} >/dev/null ; then
 echo zfs for ${GROUP} does not exist.
 exit 2
fi

if [ ! -d "${DIR}" ] ; then
 echo ${DIR} does not exist.
 exit 3
fi

if [ -f /${GROUPZFS}/${GROUP}/config/samba/shares/${NAME} ] ; then
 echo Samba config file for ${NAME} already exists at /${GROUPZFS}/${GROUP}/config/samba/shares/${NAME}
 exit 4
fi

# Create the share
cat <<EOF >/${GROUPZFS}/${GROUP}/config/samba/shares/${NAME}
# Samba share configured automatically by $0 on `date`
[${NAME}]
 comment = ${NAME} share
 path = ${DIR}
 ${SHAREOPTS:+include=/${CONFIGZFS}/samba/common/${SHAREOPTS}}
 include=/${CONFIGZFS}/samba/common/share-general
 valid users = +AD+${GROUP}-users
 ${COMMUNAL:+create mask = 0660}
 ${COMMUNAL:+directory mask = 0770}
 ${COMMUNAL:+force group = +AD+${GROUP}-users}
EOF
